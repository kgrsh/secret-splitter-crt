'use strict'
window.addEventListener('load', main)
var primes="101    103    107    109    113 127    131    137    139    149    151    157    163    167    173 179    181    191    193    197    199    211"
		.split(" ").filter(a => Boolean(a)).map(a => Number(a))

function apply_mathjax(){
	MathJax.Hub.Queue(["Typeset",MathJax.Hub])
}

function add_element(parentElement,spec,text){
	var tag=""
	var classes=[]
	var id=""
	try {var tag=spec.match(/^\w*\b/g)} catch {}
	try {var classes=spec.match(/\..+?\b/g).map(a=> a.slice(1))} catch {}
	try {var id=spec.match(/\#.+?\b/g).map(a=> a.slice(1)).pop()} catch {}
	var newnode= document.createElement(tag)
	newnode.className=classes.join(" ")
	newnode.id=id
	newnode.innerText=text
	parentElement.appendChild(newnode)
	
}

function clear_children(parentElement){
	while (parentElement.children[0]) {parentElement.removeChild(parentElement.children[0])}
}

function make_range(num){
	return Array.from(Array(num)).map( (a,i) => i)
}

function calculate_coefficient(tuple){
	var prime=tuple[0]
	var product=tuple[1]
	return make_range(prime).filter(i => ( ( (product * i) % prime) == 1))[0]
}

function calculate_coefficients(primes){
}

function update() {
	var shares=document.querySelector('#shares').valueAsNumber
	var threshold=document.querySelector('#threshold').valueAsNumber
	if (threshold > shares)
	{
		threshold = shares
		document.querySelector('#threshold').valueAsNumber=shares
	}
	if (threshold <  document.querySelector('#threshold').min)
	{
		threshold = document.querySelector('#threshold').min
		document.querySelector('#threshold').valueAsNumber= document.querySelector('#threshold').min
	}

	var used_primes=primes.slice(0,shares)
	var boundary_factors=[
		used_primes.slice(-(threshold-1)),
		used_primes.slice(0,threshold),
	]
	var boundaries= boundary_factors.map(el => el.reduce((a,c) => a*c))
	if (boundaries[0] > boundaries[1]) {alert('Primes too small!')}
	var secret= Math.round(boundaries[0]+(boundaries[1]-boundaries[0])*Math.random())
	var remainders= used_primes.map(el => secret % el)
	var coefficients= calculate_coefficients(used_primes)

	var output_primes=document.querySelector('#output-primes')
	var output_boundaries=document.querySelector('#output-boundaries')
	var output_secret=document.querySelector('#output-secret')
	var output_remainders=document.querySelector('#output-remainders')

	clear_children(output_primes)
	clear_children(output_boundaries)
	clear_children(output_secret)
	clear_children(output_remainders)
	used_primes.map((el,i) => add_element(output_primes,'p','\\( P_{'+String(i)+'}\\) = '+String(el)))
	add_element(output_boundaries,'p','\\( S \\geqslant ( '+boundary_factors[0].join(' \\cdot ')+' )  \\)')
	add_element(output_boundaries,'p','\\( S \\leqslant ( '+boundary_factors[1].join(' \\cdot ')+' )  \\)')
	add_element(output_boundaries,'p','\\( '+String(boundaries[0])+'\\leqslant S \\leqslant'+String(boundaries[1])+' \\)')
	add_element(output_secret,'p','\\( S = '+String(secret)+' \\)')
	remainders.map((el,i) => add_element(output_remainders,'p','\\( a_{'+String(i)+'} = '+String(el)+' \\)'))
	apply_mathjax()
}

function  update_threshold() {
	var shares=document.querySelector('#shares').valueAsNumber
	document.querySelector('#threshold').max=shares
}

function main(){
	document.querySelector('#shares').addEventListener('change',update_threshold)
	document.querySelector('#update-button').addEventListener('click',update)
	update_threshold()
	update()

}
