'use strict'
window.addEventListener('load', main)

function apply_mathjax(){
	MathJax.Hub.Queue(["Typeset",MathJax.Hub])
}

function add_element(parentElement,spec,text){
	var tag=""
	var classes=[]
	var id=""
	try {var tag=spec.match(/^\w*\b/g)} catch(err) {}
	try {var classes=spec.match(/\..+?\b/g).map(a=> a.slice(1))} catch(err) {}
	try {var id=spec.match(/\#.+?\b/g).map(a=> a.slice(1)).pop()} catch(err) {}
	var newnode= document.createElement(tag)
	newnode.className=classes.join(" ")
	newnode.id=id
	newnode.innerText=text
	parentElement.appendChild(newnode)
	
}

function clear_children(parentElement){
	while (parentElement.children[0]) {parentElement.removeChild(parentElement.children[0])}
}

function make_range(num){
	return Array.from(Array(num)).map( (a,i) => i)
}

function _calculate_coefficient(tuple){
	var [prime,product]=tuple
	console.log(prime,product)
	return [make_range(prime).filter(i => ( ( (product * i) % prime) == 1))[0],product]
}

function _calculate_coefficients(primes){
	return primes.map((a,i) => 
		calculate_coefficient([ 
			primes[i],
			primes.filter((b,j) => i!=j).reduce((a1,c) => a1*c)
		])
	)
}

function update(){
	input=JSON.parse(document.querySelector('#input').value)
	var used_primes=input.map(a => a[0])
	var remainders=input.map(a => a[1])
	var total_product= used_primes.reduce((a,c) => a.times(c),Big(1))
	var products= used_primes.map(a => total_product.div(a))
	var coefficients= used_primes.map((c,i) => 
		make_range(c).filter(a => ( products[i].times(a).mod(c) ==1 ))[0]
	)
	var result= products.map((a,i) => a.times(remainders[i]).times(coefficients[i]))
		.reduce((a,c) => a.plus(c),Big(0))
		.mod(total_product).toString()
	/*
	var coefficients_products = calculate_coefficients(used_primes)
	var coefficients= coefficients_products.map( a=> a[0])
	var products= coefficients_products.map( a=> a[1])
	var total_product= used_primes[0] * products[0]
	var result= products.map((el,i) =>
		el*	coefficients[i]* remainders[i]
	).reduce( (a,el) => a+el ) % total_product
	var prime_sets=used_primes.map( (a,i) => 
		[].concat(
			remainders[i],
			coefficients[i],
			used_primes.filter((b,j) => i!=j)
		)
	)
	*/

	
	console.log(used_primes)
	console.log(remainders)
	console.log(total_product)
	console.log(products)
	console.log(coefficients)
	console.log(result)
	

	var output_primes=document.querySelector('#output-primes')
	var output_remainders=document.querySelector('#output-remainders')
	var output_coefficients=document.querySelector('#output-coefficients')
	var output_calculations=document.querySelector('#output-calculations')
	clear_children(output_primes)
	clear_children(output_remainders)
	clear_children(output_coefficients)
	clear_children(output_calculations)

	used_primes.map((el,i) => add_element(output_primes,'p ','\\( P_{'+String(i+1)+'}\\) = '+String(el)))
	remainders.map((el,i) => add_element(output_remainders,'p ','\\( a_{'+String(i+1)+'}\\) = '+String(el)))
	coefficients.map((el,i) => add_element(output_coefficients,'p ','\\( C_{'+String(i+1)+'}\\) = '+String(el)))
	add_element(output_calculations,'p','\\( S = '+String(result)+'\\)' )

	apply_mathjax()
	
}

function main(){
	document.querySelector('#update-button').addEventListener('click',update)
	update()
}
